/*
	MostWanted - Wild Digital Agency
	http://visibleScreen()w.mostwanted-agency.net
	info@mw-a.net
*/

// VALIDATE EMAIL
function validateEmail(email) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if( !emailReg.test( email ) ) {
		return false;
	} else {
		return true;
	}
}

// POPIN FUNCTION
var popinOpen = function(popinID) {
	//use like this:
	//popinOpen("popin-id");
	//or close popinOpen();
	var $popin = popinID !== undefined ? $('.popin#'+popinID) : $('.popin');
	var fadeSpeed = $('html').hasClass('lt-ie9') ? 1 : 300;
	var animationSpeed = Modernizr.touch ? 1 : 400;
	if (popinID.length > 0 && popinID !== undefined) {
		$popin.parent('.popinContainer').fadeIn(fadeSpeed, function() {
			$popin.show().stop().animate({
				top: '20%'
			}, animationSpeed, "easeOutExpo");
		});
	} else {
		$popin.stop().animate({
			top: '-100%'
		}, animationSpeed, "easeInExpo", function() {
			$popin.parent('.popinContainer').fadeOut(fadeSpeed);
		});
	}
};

// CHECK FORM
var checkForm = function(form) {
	//use like this:
	//if (!checkFrom(form)) { ... }
	var error = 0;
	form.find('.mandatory').each(function(index, elem) {
		var elem = $(elem);
		var parent = elem.parent('p');
		if (elem.is('.email')) {
			if (!(validateEmail(elem.val()) && elem.val().length > 0 && elem.val() !== elem.attr('placeholder'))) {
				error++;
			}
		} else if (elem.is('.inputText, .select, .textarea')) {
			if (elem.val().length < 1 || elem.val() === elem.attr('placeholder')) {
				error++;
			}
		}
		if (error !== 0) {
			elem.addClass('error');
			parent.addClass('error');
		} else {
			elem.removeClass('error');
			parent.removeClass('error');
		}
	});
	if (error !== 0) {
		return false;
	} else {
		return true;
	}
};



// // SMOOTH SCROOL
// var jump = function(event) {
// 	event.preventDefault();
// 	var target = $(this).attr("href");
// 	var top;
// 	var speed = 2000;

// 	if (target === "#") {
// 		top = 0;
// 		speed = 1;
// 	} else {
// 		top = $(target).offset().top;
// 	}

// 	$('html, body').stop().animate({
// 		scrollTop: top
// 	},speed, "easeInOutExpo", function() {
// 		location.hash = target;
// 	});
// };

// LARGERST FOR TWO ELEMENTS
var largest = function(a, b) {
	b.css('min-height', '100%');
	a.css('min-height', '100%');
	return a.height() > b.height() ? b.css('min-height', a.height()) : a.css('min-height', b.height());
};

var largest2 = function(a, b) {
	var maxSize = 0;
	b.css('height', 'auto');
	a.css('height', 'auto');

	if (a.outerHeight() > b.outerHeight()) {
		maxSize = a.outerHeight();
	} else {
		maxSize = b.outerHeight();
	}
	b.outerHeight(maxSize);
	a.outerHeight(maxSize);
};


// MENU OPENER
var menuOpener = function(menu,elems,mobileElems) {
	var $menu = $(menu);
	var $elems = $(elems);
	var $mobileElems = $(mobileElems);
	if (!$menu.hasClass('opened')) {
		$menu.addClass('opened');
		$elems.addClass('open');
		$mobileElems.addClass('open');
	} else {
		$menu.removeClass('opened');
		$elems.removeClass('open');
		$mobileElems.removeClass('open');
	}
};

// MOVE ARROW IN SLIDE
var moveArrow = function(number, start, arrow) {
	var speed = Modernizr.touch ? 250 : 1000;
	if (start) { speed = 1; }
	if (number === undefined) { number = 0; }
	var arrowHeight = $(arrow).height();
	var thumbsHeight = $('.thumbsNav li:first').height();
	var positionTop = $('.thumbsNav li').eq(number).position().top;
	$('.thumbsNav li').removeClass('active');
	$('.thumbsNav li').eq(number).addClass('active');
	$(arrow).stop().animate({
		top: positionTop + (thumbsHeight/2) - (arrowHeight/2)
	},speed, "easeInOutExpo");
};

// CHANGE LANG
var langagueOpen = function(lang) {
	var $lang = $(lang);
	if ($lang.hasClass('open')) {
		$lang.find(".langChoice").slideUp("fast", function() {
			$lang.removeClass('open');
		});
	} else {
		$lang.find(".langChoice").slideDown("fast");
		$lang.addClass('open');
	}
};

// GET THE SCROLLBARS WIDTH
var getScrollBarDimensions = function() {
	var elm = document.documentElement.offsetHeight ? document.documentElement : document.body,
		curX = elm.clientWidth,
		curY = elm.clientHeight,
		hasScrollX = elm.scrollWidth > curX,
		hasScrollY = elm.scrollHeight > curY,
		prev = elm.style.overflow,
		r = {
			vertical: 0,
			horizontal: 0
		};
	if( !hasScrollY && !hasScrollX ) {
		return r;
	}
	elm.style.overflow = "hidden";
	if( hasScrollY ) {
		r.vertical = elm.clientWidth - curX;
	}
	if( hasScrollX ) {
		r.horizontal = elm.clientHeight - curY;
	}
	elm.style.overflow = prev;
	return r;
}


//START 
$(document).ready(function() {

	var currentSlide = 0;
	var mobileBreak = 568;
	var ipadBreak = 768;
	var $main = $('.main');
	var $menu = $('.header .mainNavigation');
	var $elems = $('.header, .main, .preFooter, .footer');
	var $mobileElems = $('.headerMobile, .pictoContainer');
	var $slideArrow = $('.arrowSlide');
	var $lang = $('.language');
	var options = {};

	var visibleScreen = function() {
		return $(window).width() + getScrollBarDimensions().vertical;
	};

	//open close item in product & pyjama pages
	var moveDetailsToItemList = function(elem) {

		if (!elem.hasClass('active')) {

			$('.items .item').removeClass('active');
			elem.addClass('active');

			var id = elem.attr('id') + "_details";
			var $listClear = $('<li class="listClear"></li>');
			var itemByLines = 4;

			if (visibleScreen() <= ipadBreak) {
				itemByLines = 3;
			}

			if (visibleScreen() <= mobileBreak) {
				itemByLines = 1;
			}

			$main.find('.items .listClear').detach();
			
			var line = Math.floor(elem.index() / itemByLines) + 1;
			var positionToClone = line*itemByLines;
			var $itemToClone = $main.find('.itemDetailsContainer > #'+id).clone();

			//console.log(positionToClone, $('.items .item').length , id, itemByLines, line, $itemToClone.length);

			if (positionToClone > $('.items .item').length) {
				positionToClone = $('.items .item').length;
			}

			//bind close button
			$itemToClone.find('.close').on('click',function(e) {
				e.preventDefault();
				$(this).parents('li').slideUp("fast");
				elem.removeClass('active');
			});

			//bind commander (-> step2) button
			$itemToClone.find('.commander').on('click', function(e) {
				e.preventDefault();
				$(this).hide();
				$(this).prev('.toggle2_step2').show(0, function() {
					resizeItemDetails();
				});
			});

			//SELECT -> CHOSEN PLUGIN
			$itemToClone.find('.selector').each(function() {
				var selectWidth = $(this).attr('data-width');
				$(this).chosen({
					disable_search: true,
					inherit_select_classes: true,
					width: selectWidth + "%"
				});
			});

			//supprimer bind
			$itemToClone.find(".supprimer").click(function(){
				$itemToClone.find('.close').trigger('click');
			});

			$listClear.append($itemToClone);
			$main.find('.items .item:nth-child('+positionToClone+')').after($listClear);
			
			$main.find('.items .listClear').show(0, function() {
				resizeItemDetails();
				$main.find('.items .listClear').hide().slideDown(300);
			});
		}
	};

	//Attatch plugin placeholder plugin
	$('input[placeholder]').placeholder();

	//HOME
	if ($main.attr('id') === "index") {
		options = {
			namespace: "noukies-",
			animation: "slide",
			direction: Modernizr.touch ? "horizontal" : "vertical",
			easing: Modernizr.touch ? "swing" : "easeInOutExpo",
			useCSS: false,
			pauseOnHover: true,
			pauseOnAction: false,
			touch: true,
			initDelay: 0,
			slideshowSpeed: 2000,
			animationSpeed: Modernizr.touch ? 250 : 800,
			controlNav: false,
			directionNav: false,
			start: function() {
				if (visibleScreen() > mobileBreak) {
					moveArrow(0, true, $slideArrow);
				}
			},
			before: function(data) {
				if (visibleScreen() > mobileBreak) {

					if(data.currentSlide+1 === data.count) {
						currentSlide = 0;
					} else {
						currentSlide = data.currentSlide + 1;
					}
					moveArrow(currentSlide, false, $slideArrow);
				}
			}
		};

		// NAV IN SLIDES BY THUMBS
		$(".thumbsNav li").on("click", function(){
			var index = $(this).index();
			$('.flexslider').flexslider(index);
			if (visibleScreen() > mobileBreak) {
				moveArrow($(this).index(), false, $slideArrow);
			}
		});

		
		// RESIZE in home
		$(window).resize(function() {
			if (visibleScreen() > mobileBreak) {
				moveArrow(currentSlide, false, $slideArrow);
			}
		});
	}

	// CREATE THE SLIDER (BASED ON OPTIONS)
	$('.flexslider').flexslider(options);


	// VALIDER EMAIL
	$('.newsSubscribe form').on('submit', function(e) {
		e.preventDefault();
	});

	// CHECK NEWSLETTER FORM
	$('.newsletterForm .submit').on('click', function(e) {
		e.preventDefault();
		var form = $(this).parents('.newsletterForm');
		if (checkForm(form)) {
			var email = form.find('.email').val();
			//Ajax post here
			//in ajax succes or something
			form.find('input').hide(0, function() {
				form.find('.thanks').fadeIn(300);
			});
		}
	});

	// CLOSE LANGUAGE ON BODY CLICK
	$("body").on("click", function(){
		if ($lang.hasClass('open')) {
			langagueOpen($lang);
		}
		if ($menu.hasClass('opened')) {
			menuOpener($menu, $elems, $mobileElems);
		}
	});

	// OPEN/CLOSE LANGUAGES
	$lang.find(".langUsed").on("click", function(e){
		e.stopPropagation();
		langagueOpen($lang);
	});

	// SET CURRENT LANG ON CLICK
	$lang.find('.langChoice').find('li').on('click', function() {
		$lang.find('.langCurrent').text($(this).text());
	});


	// JQUERY MOBILE MENU
	$(".pictoMenu").on("click", function(e){
		e.preventDefault();
		e.stopPropagation();
		menuOpener($menu, $elems, $mobileElems);

		$(document).scrollTop(0);
	});

	//RESIZE MENU
	if (visibleScreen() <= mobileBreak) {
		largest($menu, $(".container"));
	}

	// RESIZE
	$(window).resize(function() {
		if (visibleScreen() > mobileBreak) {
			$(".header .mainNavigation, .container").css('min-height','100%');
		} else {
			largest($menu, $(".container"));
		}
	});



	//CHARACTERS
	if ($main.attr('id') === "characters") {

		//show character details from list
		$main.find('.charactersList > li > a').on('click', function(e) {
			var link = $(this);
			var id = link.parents('.charactersDetails').attr('id');
			var target = link.attr("href");
			var top = $(target).offset().top;
			var speed = 500;

			e.preventDefault();

			var target = $(this).attr('data-target');
			$main.find('.charactersList > li').removeClass('active');
			link.parent('li').addClass('active');
			$main.find('.charactersDetailsContainer .charactersDetails:not(#'+target+')').fadeOut(300);
			
			if (visibleScreen() > mobileBreak) {
				$main.find('.charactersDetails#'+target).show(0, function() {
					$(this).find('.col').each(function() {
						if ($(this).next().is('.col')) {
							largest2($(this), $(this).next());
						}

						if ($(this).find('img').length > 1) {
							$(this).find('img:last').addClass('second');
						}
					});
				});
				$('html, body').stop().animate({
					scrollTop: top
				},speed, "easeInOutExpo", function() {
					location.hash = target;
				});
				
			} else {
				$main.find('.charactersDetails#'+target).show(0, function() {
					$('html, body').stop().animate({
						scrollTop: top
					},speed, "easeInOutExpo", function() {
						location.hash = target;
					});
					$(this).find('.col').css('height','auto');
				});
			}
		});

		//close details
		$main.find('.charactersDetails .close').on('click', function(e) {
			e.preventDefault();
			var id = $(this).parents('.charactersDetails').attr('id');
			var speed = 500;

			$main.find('.charactersList > li').removeClass('active');
			$(this).parents('.charactersDetails').fadeOut(300, function() {
				var posTop = $main.find('.charactersList .'+id).position();
				if (posTop === undefined) {
					posTop = 0;
				} else {
					posTop = posTop.top;
				}
				$('html, body').stop().animate({
					scrollTop: posTop
				},speed, "easeInOutExpo");
			});
		});

		$(window).resize(function() {
			if (visibleScreen() > ipadBreak) {
				$main.find('.charactersDetails:not(:hidden) .col').each(function () {
					if ($(this).next().is('.col')) {
						largest2($(this), $(this).next());
					}
				});
			} else {
				$main.find('.charactersDetails:not(:hidden) .col').css('height','auto');
			}
		});

	} // end characters






	//histoire
	if ($main.attr('id') === "histoire") {
		if (visibleScreen() > ipadBreak) {
			$main.find('.annee').each(function() {
				largest2($(this).find('.col.left'), $(this).find('.col.right'));
			});
		} else {
			$main.find('.annee .col').css('height','auto');
		}

		$(window).resize(function() {
			if (visibleScreen() > ipadBreak) {
				$main.find('.annee').each(function() {
					largest2($(this).find('.col.left'), $(this).find('.col.right'));
				});
			} else {
				$main.find('.annee .col').css('height','auto');
			}
		});
	}
	//end histoire





	//PRODUCTS GENERICS
	if ($main.attr('id') === "products") {

		//section specific resizing elements
		var resizeItemDetails = function() {
			if (visibleScreen() > ipadBreak) {
				largest2($main.find('.listClear .col.left'),$main.find('.listClear .col.right'));
			} else {
				$main.find('.listClear .col').css('height','auto');
			}
		};

		//Idem au resize
		$(window).resize(function() {
			$('.listClear .close').trigger('click');
			resizeItemDetails();
		});

		//items toggle details
		$main.find('.items .item').on('click', function(e) {
			e.preventDefault();
			moveDetailsToItemList($(this));
		});

	}//end generic products















	// Pyjama party
	if ($main.attr('id') === "pyjamaParty") {

		var resizeItemDetails = function() {
			//les deux blocs de l'intro on la même hauteur
			if (visibleScreen() > ipadBreak) {
				largest2($main.find('#pyjamaIntro .left_square'),$main.find('#pyjamaIntro .right_square'));
				if ($('.listClear').length > 0) {
					largest2($main.find('.listClear .left_square'),$main.find('.listClear .right_square'));
				}
			} else {
				$main.find('#pyjamaIntro .left_square, #pyjamaIntro .right_square').css('height','auto');
				$main.find('.listClear .left_square, .listClear .right_square').css('height','auto');
			}
		};

		resizeItemDetails();

		//Idem au resize
		$(window).resize(function() {
			resizeItemDetails();
		});

		//attatch checkbox plugin
		$main.find('.checkbox').checkbox();
		
		// order button
		$main.find('#bt_step1').on('click', function(e) {
			e.preventDefault();
		});

		//items toggle details
		$main.find('.items .item').on('click', function(e) {
			e.preventDefault();
			moveDetailsToItemList($(this));
		});

		// Pyjama-party-order.html  supprimer pyjama choisis 
		$main.find('.delete_button').on('click', function(e) {
			$(this).parents('.panelArticle').hide('slow', function() {
				if ($main.find('.panelArticle:not(:hidden)').length < 1) {
					$main.find('.no-article').slideDown(300);
				}
			});
		});

		//SELECT -> CHOSEN PLUGIN
		$main.find('#select_pyjama_form .selector').each(function() {
			var selectWidth = $(this).attr('data-width');

			$(this).chosen({
				disable_search: true,
				inherit_select_classes: true,
				width: selectWidth + "%"
			});
		});

		$main.find('#adress_form .selector').chosen({
			disable_search: true,
			inherit_select_classes: true,
			width: $main.find('#adress_form .selector').attr('data-width') + "%"
		});

		$main.find('#pyjama_filters #select_nombre').chosen({
			disable_search: true,
			inherit_select_classes: true,
			width: '100%'
		});

		$main.find("#bt_step1").click(function(){
			$main.find(".step1").fadeOut("fast", function() {
				$main.find(".step2").fadeIn("fast");
			});
			$(".find_shop").slideUp();
		});
		
		$main.find("select#select_pyjama").change(function(){
			$main.find(".pyjama_filters, .pyjama_toggle1").slideDown(500);
			resizeItemDetails();
			$main.find('.step3').fadeIn(300, function() {
				resizeItemDetails();
			});
		});

	}//end pyjama party














	//Contact page
	if ($main.attr('id') === "contact") {
		$main.find('.checkbox').checkbox();

		$main.find('.contactForm .submit').on('click', function() {
			if(!checkForm($main.find('.contactForm'))) {
				e.preventDefault();
			}
		});
		
		if (visibleScreen() > ipadBreak) {
			largest2($main.find('.col.left'), $main.find('.col.right'));
		} else {
			$main.find('.col').css('height','auto');
		}

		$(window).resize(function() {
			if (visibleScreen() > ipadBreak) {
				largest2($main.find('.col.left'), $main.find('.col.right'));
			} else {
				$main.find('.col').css('height','auto');
			}
		});
	}//end contact










	// SOS Noukies
	if($main.attr('id') === "sosNoukies") {
		$main.find('.sosTools .selector').chosen({
			disable_search: true,
			inherit_select_classes: true,
			width: "100%"
		});

		//items toggle details
		$main.find('.items .item').on('click', function(e) {
			e.preventDefault();
			moveDetailsToItemList($(this));
		});

		if (visibleScreen() > ipadBreak) {
			largest2($main.find('.sosHeader .col.left'), $main.find('.sosHeader .col.right'));
			largest2($main.find('.sosTools .col.left'), $main.find('.sosTools .col.right'));
		} else {
			$main.find('.col').css('height','auto');
		}

		//section specific resizing elements
		var resizeItemDetails = function() {
			if (visibleScreen() > ipadBreak) {
				largest2($main.find('.listClear .col.left'),$main.find('.listClear .col.right'));
			} else {
				$main.find('.listClear .col').css('height','auto');
			}
		};

		$(window).resize(function() {
			resizeItemDetails();
			
			if (visibleScreen() > ipadBreak) {
				largest2($main.find('.sosHeader .col.left'), $main.find('.sosHeader .col.right'));
				largest2($main.find('.sosTools .col.left'), $main.find('.sosTools .col.right'));
			} else {
				$main.find('.col').css('height','auto');
			}
		});
	}
	//END SOS NOUKIES






	// (NIGHTWEAR) PLUGIN MASONRY
	if ($('.js-masonry').length > 0) {

		var $container = $('.js-masonry');
		var img = $container.find('img');
		var imgCount = img.length;

		$container.css('opacity','0');

		function masonrySettings() {
			$container.masonry({
				itemSelector: '.item'
			}).delay(300).animate({
				opacity: 1
			},600);
		}

		setTimeout(function() {
			masonrySettings();
		},100);

		img.load(function() {
			if (imgCount === 1) {
				// initialize
				masonrySettings();
				
			} else {
         		imgCount--;
			}
		});
	}//end night/day wear






	// ESPACE CONSEIL Toggle
	if ($main.attr('id') === "espaceConseil") {
		if (visibleScreen() > mobileBreak) {
			largest2($main.find('.espaceConseil .col.left'), $main.find('.espaceConseil .col.right'))
		} else {
			$main.find('.espaceConseil .col').css('height','auto');
		}

		$main.find('.toggleTitles').on('click',function(e) {
			e.preventDefault();
			var title = $(this);
			var speed = 300;

			if (!title.hasClass('open')) {
				$main.find('.toggleTitles.open .toggle_content').slideUp(speed, function() {
					$(this).parents('.open').removeClass('open');
				});
				title.find('.toggle_content').slideDown(speed);
				title.addClass('open');
			} else {
				title.find('.toggle_content').slideUp(speed,function() {
					title.removeClass('open');
				});
			}
		});

		$main.find('.toggleTitles .toggle_content').on('click',function(e) {
			e.stopPropagation();
		});

		$(window).resize(function(event) {
			if (visibleScreen() > mobileBreak) {
				largest2($main.find('.espaceConseil .col.left'), $main.find('.espaceConseil .col.right'));
			} else {
				$main.find('.espaceConseil .col').css('height','auto');
			}
		});
	}//end espace conseil



	//NEWSLETTER
	if ($main.attr('id') === "newsletter") {
		
		// Radio
		$main.find('.radioButton').radioButton();

		// Select
		$main.find('.selector').each(function() {
			var search = true;
			if ($(this).attr('id') === "country_id") {
				search = false;
			}

			//console.log($(this).attr('id'), search);

			$(this).chosen({
				disable_search: search,
				inherit_select_classes: true,
				width: $(this).attr('data-width') + "%"
			});
		});

		//Childrens
		$main.find(".childrens .inputText").on('keypress', function(e) {
			if (!$(this).hasClass('open') && $(this).val().length > 0) {
				$(this).addClass('open');
				$(this).parents(".childrens").next(".childrens").show(0, function() {
					ajustement();
					$(this).hide().slideDown(300);
				});
			}
		 });

	}
	// -------- END NEWSLETTER -------- //


	//Valeurs & naissances
	if ($main.attr('id') === "valeurs" || $main.attr('id') === "naissances" || $main.attr('id') === "club" || $main.attr('id') === "newsletter") {

		var img = $main.find('.col img');
		var imgCount = img.length;

		//Ajustement spécial pour ces deux pages :
		var ajustement = function() {

			if (visibleScreen() > mobileBreak) {
				//reset
				$main.find('.col, .col .title').css('height','auto');

				//set sizes
				largest2($main.find('.col.left'), $main.find('.col.right'));

				//step2 - picture always on bottom
				var colLeftHeight = parseInt($main.find('.col.left').outerHeight(),10);
				var titleHeight = parseInt($main.find('.col.left .title').outerHeight(),10);
				var imgHeight = parseInt($main.find('.col.left img').outerHeight(),10);
				var difference = colLeftHeight - imgHeight;

				if (colLeftHeight > (titleHeight + imgHeight)) {
					$main.find('.col.left .title').outerHeight(difference);
				}
			} else {
				$main.find('.col').css('height','auto');
			}
		};

		//one at start
		setTimeout(function() {
			ajustement();
		},1);

		//another after picture load
		img.load(function() {
			if (imgCount === 1) {
				ajustement();
			} else {
				imgCount--;
			}
		});
		
		//on resize
		$(window).resize(function() {
			ajustement();
		});
	}
	// end valeurs & naissance
	


	// SEARCH RESULTS
	if ($main.attr('id') === "searchResults") {
		$main.find('.searchResultsHeader .tab').on('click', function(e) {
			e.preventDefault();

			if (!$(this).hasClass('active')) {
				var target = $(this).attr('data-target');				
				
				$main.find('.searchResultsHeader .tab').removeClass('active');
				$(this).addClass('active');

				$main.find('.searchSections:not(#'+target+')').slideUp(300, function() {
					$main.find('.searchSections#'+target).slideDown(300);
					$main.find('.searchSections').removeClass('active');
					$(this).addClass('active');
				});
			}
		});

		$main.find('.searchResultsHeader .tab:first').trigger('click')
	}
	// END SEARCH RESULTS

	


	//Dear Locator Slider
	if($main.attr('id') === "dealerLocator") {
		
		var options = {
			namespace: "noukies-",
			animation: "slide",
			direction: "horizontal",
			easing: Modernizr.touch ? "swing" : "easeInOutExpo",
			useCSS: false,
			pauseOnHover: true,
			pauseOnAction: false,
			touch: true,
			smoothHeight: true,
			animationLoop: false,
			slideshowSpeed: 3000,
			animationSpeed: Modernizr.touch ? 250 : 1000,
			controlNav: true,
			directionNav: true,
			slideshow: false
		}

		if ($main.find('.slides').length > 0) {
			$main.find('.resultsContainer, .slidesList').flexslider(options);
		}
	}


});
