// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

(function($) {
    //Placeholder for old browser
    $.fn.placeholder = function() {
        this.focus(function() {
            var input = $(this);
            if (input.val() === input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function() {
            var input = $(this);
            if (input.val() === '' || input.val() === input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        }).blur().parents('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                var input = $(this);
                if (input.val() === input.attr('placeholder')) {
                    input.val('');
                }
            });
        });
        return this;
    }

    //Checkbox with style & accessible
    $.fn.checkbox = function() {
        var input = this.find('input[type=checkbox]');
        input.each(function() {
            if ($(this).is(':checked')) $(this).parent('label.checkbox').addClass('checked');
            $(this).focus(function() {
                $(this).parent('label.checkbox').addClass('hover');
            }).blur(function() {
                $(this).parent('label.checkbox').removeClass('hover');
            }).on('change', function() {
                if($(this).is(':checked')) {
                    $(this).parent('label.checkbox').addClass('checked');
                } else {
                    $(this).parent('label.checkbox').removeClass('checked');
                }
            });
        });
        return this;
    };

    //Radio with style & accessible
    $.fn.radioButton = function() {
        var that = this;
        var input = this.find('input[type=radio]');
        var name = input.attr('name');
        input.each(function() {
            if ($(this).is(':checked')) $(this).parent('label.radioButton').addClass('checked');
            $(this).focus(function() {
                $(this).parent('label.radioButton').addClass('hover');
            }).blur(function() {
                $(this).parent('label.radioButton').removeClass('hover');
            }).on('change', function() {
                that.find('input[name='+name+']').parent('label.radioButton').removeClass('checked');
                if($(this).is(':checked')) {
                    $(this).parent('label.radioButton').addClass('checked');
                } else {
                    $(this).parent('label.radioButton').removeClass('checked');
                }
            });
        });
        return this;
    };
}(jQuery));
